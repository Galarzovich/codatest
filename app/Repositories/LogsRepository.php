<?php


namespace App\Repositories;


use App\Core\AppRepository;
use App\Models\Log;
use DateTime;;


class LogsRepository extends AppRepository
{
    public function LogStore($user, $type, $item, $data , $caption):bool
    {

        $array = [
            'user_id' => $user,
            'type_id' => $type,
            'item_id' => $item,
            'data' => $data,
            'caption' => $caption
        ];

        $log = new Log($array);

        $log->save();

        if (!$log) {
            return false;
        }
        return true;
    }
}
