<?php

namespace App\Policies;

use App\Core\Enums\EnumUsers;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UsersPolicy
{
    use HandlesAuthorization;

    public function saveUser(User $user)
    {
        $userAuth = [
            EnumUsers::EDITOR,
        ];

        return in_array($user->role, $userAuth) ;
    }




}
