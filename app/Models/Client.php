<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $table = 'client';
    protected $guarded = [];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $with = [
        'projects'
    ];

    public function scopeFirstname($query, $firstName)
    {
        if ($firstName) {
            return $query->where('firstname', 'like', "%$firstName%");
        }
    }

    public function scopeLastname($query, $lastName)
    {
        if ($lastName) {
            return $query->where('lastname', 'like', "%$lastName%");
        }
    }

    public function scopeEmail($query, $email)
    {
        if ($email) {
            return $query->where('email', 'like', "%$email%");
        }
    }

    public function scopePhone($query, $phone)
    {
        if ($phone) {
            return $query->where('email', 'like', "%$phone%");
        }
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'client_id');
    }

}

