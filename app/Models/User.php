<?php

namespace App\Models;

use App\Core\Enums\EnumUsers;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'mia_user';

    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'photo',
        'phone',
        'facebook_id',
        'role',
        'password',
        'status',
        'is_notification',
        'caption'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];



}
