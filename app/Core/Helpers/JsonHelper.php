<?php


namespace App\Core\Helpers;


use Karriere\JsonDecoder\Bindings\FieldBinding;
use Karriere\JsonDecoder\ClassBindings;
use Karriere\JsonDecoder\JsonDecoder;
use Karriere\JsonDecoder\Transformer;
use stdClass;

abstract class JsonHelper
{
    /***
     * @param string|array|stdClass $data : json or array
     * @param string $classType
     * @param array $bindings
     * @return mixed|null
     */
    public static function parseClass($data, string $classType, array $bindings = [])
    {
        $json = $data;
        if(!is_string($data)){
            $json = json_encode($data);
        }
        if(!$json){
            return null;
        }

        $decoder = new JsonDecoder(true,true);
        if(count($bindings) > 0){
            // tiene bindings--> creamos un transformer
            $transformer = new class($classType,$bindings) implements Transformer{
                private $classType;
                private $bindings;
                public function __construct($classType, array $bindings)
                {
                    $this->classType = $classType;
                    $this->bindings = $bindings;
                }

                public function register(ClassBindings $classBindings)
                {
                    foreach ($this->bindings as $binding) {
                        $classBindings->register($binding);
                    }
                }

                public function transforms()
                {
                    return $this->classType;
                }
            };

            $decoder->register($transformer);

        }


        return $decoder->decode($json, $classType);
    }
}
