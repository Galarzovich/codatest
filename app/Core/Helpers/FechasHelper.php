<?php


namespace App\Core\Helpers;


use DateInterval;
use DateTime;

abstract class FechasHelper
{
    public const DEFAULT_FORMAT = 'Y-m-d';
    public const LOCAL_FORMAT = 'd/m/Y H:i:s';
    public const LOCAL_FORMAT_DATE = 'd/m/Y';
    public const DAY_FORMAT = 'd';

}
