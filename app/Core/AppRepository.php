<?php


namespace App\Core;


class AppRepository
{
    private $errorMessage = "";
    private $errorDetails = [];
    private $logs = [];

    /**
     * @return array
     */
    public function getLogs(): array
    {
        return $this->logs;
    }

    /**
     * @param string $log
     */
    public function addLog(string $log)
    {
        $this->logs[] = $log;
    }


    /***
     * Devuelve el mensaje de error del último error
     * @return string
     */
    public function getErrorMessage():string
    {
        return $this->errorMessage;
    }

    /***
     * Devuelve una lista con detalles del último error
     * @return array
     */
    public function getErrorDetails()
    {
        return $this->errorDetails;
    }

    /***
     * @param string $errorMessage
     * @param string|array|null $errorDetails
     */
    protected function setError(string $errorMessage, $errorDetails = null)
    {
        $this->errorMessage = $errorMessage;
        if(is_string($errorDetails)){
            $this->errorDetails = [$errorDetails];
        }elseif (is_array($errorDetails)){
            $this->errorDetails = $errorDetails;
        }else{
            $errorDetails = []; //reset for previews data
        }
    }
}
