<?php


namespace App\Core\Enums;


use BenSampo\Enum\Enum;

final class EnumUsers extends Enum
{
    const ADMIN          = 1;
    const EDITOR  = 2;
}
