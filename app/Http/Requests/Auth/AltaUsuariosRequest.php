<?php

namespace App\Http\Requests\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class AltaUsuariosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'     => 'required|string',
            'lastname'     => 'required|string',
            'email'    => 'required|string|email|unique:mia_user',
            'photo'    => 'image|mimes:jpeg,png,jpg|max:2048',
            'phone'    => 'string',
            'facebook_id'    => 'string',
            'role'    => 'required|string',
            'password' => 'required|string|confirmed',
            'status'    => 'string',
            'is_notification'    => 'required|string',
            'caption'    => '',


        ];
    }

    public function messages()
    {
        return [
            'firstname.required'     => 'Por favor, ingrese un nombre.',
            'lastname.required'     => 'Por favor, ingrese un nombre.',
            'email.required'    => 'Se esperaba un email',
            'email.unique'    => 'Ya existe una cuenta registrada con ese email.',
            'password.required' => 'Se esperaba un password',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
