<?php

namespace App\Http\Controllers;

use App\Core\Enums\EnumUsers;
use App\Core\Helpers\FechasHelper;
use App\Http\Requests\Edit\AssignProjectRequest;
use App\Http\Requests\Make\MakeClientRequest;
use App\Models\Client;
use App\Models\Project;
use App\Repositories\LogsRepository;
use App\Tools\ApiMessage;
use GuzzleHttp\Promise\Create;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ClientController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new LogsRepository();
    }


    public function store(MakeClientRequest $request)
    {
        $res = new ApiMessage();

        $user = Auth::user();//get data user logged
        $data = $request->validated();

        if($user->role != EnumUsers::EDITOR)
        {
            return $res->setCode(403)->setMessage("Unauthorized user for this action")->send();
        }

        $client = new Client($data);

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $destinationPath = 'images/client_profile';
            $filename = time() . '-' . $file->getClientOriginalName();
            $load = $file->move($destinationPath,$filename);
            $client->photo = $destinationPath .'/'. $filename;
        }


        try {
            $client->saveOrFail();

            $message = "Client ".$client->id." has been successfully created";
            $log = $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);

            if (!$log)
            {
                throw new \Exception('Internal error, please try again');
            }


            $res->setMessage($message)->setData($client);

        } catch (\Throwable $e) {
            $message = "Client ".$client->id." could not be created. Reason: ".$e->getMessage();
            $log = $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);
            return $res->setCode(409)->setMessage($e->getMessage())->send();
        }

        return $res->setCode(201)->send();
    }


    public function show(int $id)
    {
        $client = Client::find($id);

        $res = new ApiMessage();

        if(!$client)
        {
            return $res->setCode(404)->setMessage("Client not found")->send();
        }
        $user = Auth::user();//get data user logged
        $message = "User ".$user->id." downloads all information from client: ".$id;
        $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);
        return $client;
    }


    public function filters(Request $request)
    {
        $res = new ApiMessage();

        $firstName = $request->get('firstname');
        $lastName = $request->get('lastname');
        $email = $request->get('email');
        $phone = $request->get('phone');

        $client = Client::firstname($firstName)->lastName($lastName)->email($email)->phone($phone)->get();

        if(count($client) == 0)
        {
            return $res->setCode(404)->setMessage("Client not found")->send();
        }

        return $res->setData($client)->setCode(200);
    }


    public function delete(int $id)
    {
        $res = new ApiMessage();
        $client = Client::find($id);//search client
        if(!$client)
        {
            return $res->setCode(404)->setMessage("Client not found")->send();
        }

        if ($client->delete == 1)
        {
            return $res->setCode(404)->setMessage("Client has been deleted at: ".$client->deleted_at)->send();
        }

        $user = Auth::user();//get data user logged
        $date = new \DateTime();//get date
        $delete = [
            'deleted_at' => $date,
            'delete' => 1,
        ];

        $client->fill($delete);
        try {
            $client->saveOrFail();
            $message = "Client ".$client->id." has been successfully deleted";

            $log = $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);

            if (!$log)
            {
                throw new \Exception('Internal error, please try again');
            }
            $res->setMessage($message)->setData($client);
        }
        catch (\Exception $e){
            $message = "Client ".$client->id." could not be deleted. Reason: ".$e->getMessage();
            $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);
            return $res->setCode(409)->addError($e->getMessage())->send();
         }
        return $res->setCode(200);
    }

    public function assignProject(AssignProjectRequest $request)
    {
        $res = new ApiMessage();
        $data = $request->validated();

        $client = Client::find($data['client_id']);//search client

        $project = Project::find($data['project_id']);//search project

        $user = Auth::user();//get data user logged

        if(!$client)
        {
            return $res->setCode(404)->setMessage("Client not found")->send();
        }
        if ($client->delete == 1)
        {
            return $res->setCode(404)->setMessage("Client has been deleted at: ".$client->deleted_at)->send();
        }
        if(!$project)
        {
            return $res->setCode(404)->setMessage("Project not found")->send();
        }

        $assign = ['client_id' => $data['client_id']];
        $project->fill($assign);
        try {
            $project->saveOrFail();
            $message = "Project: ".$project->id." assigned to client: ".$client->id;
            $log = $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);

            if (!$log)
            {
                throw new \Exception('Internal error, please try again');
            }

            $res->setMessage($message)->setData($client);
        }
        catch (\Exception $e){
            $message = "Client ".$client->id." could not be assigned. Reason: ".$e->getMessage();
            $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);
            return $res->setCode(409)->addError($e->getMessage())->send();
        }

        return $res;

    }

    public function dash()
    {
        $res = new ApiMessage();
        $user = Auth::user();//get data user logged
        $sql = "select created_at as day, COUNT(1) as users from client where created_at >= current_date - 7 group by date(created_at)";
        $consult = DB::connection('mysql')->select(DB::raw($sql));
        $array = [];
        foreach ($consult as $item)
        {
            $item->day = substr($item->day, 0, -9);
        }


        $message = "User ".$user->id." downloaded list of users uploaded in the last 7 days ";
        $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);

        return $res->setData($consult)->setCode(200);
    }



}
