<?php

namespace App\Http\Controllers;


use App\Http\Requests\Make\MakeProjectRequest;
use App\Models\Project;
use App\Repositories\LogsRepository;
use App\Tools\ApiMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProjectController extends Controller
{

    protected $repo;

    public function __construct()
    {
        $this->repo = new LogsRepository();
    }
    public function index(Request $request)
    {
        $respuesta = new ApiMessage($request);
        $lista = Project::all();
        $respuesta->setData($lista);
        return $respuesta->send();
    }


    public function store(MakeProjectRequest $request)
    {

        $res = new ApiMessage();

        $user = Auth::user();//get data user logged
        $userId =  $user->id;
        $data = $request->validated();


        $project = new Project($data);

        $project->user_id = $userId;

        try {
            $project->saveOrFail();

            $message = "Project ".$project->id." has been successfully created";
            $log = $this->repo->LogStore($userId,$user->role,'0',$message, $user->caption);

            if (!$log)
            {
                throw new \Exception('Internal error, please try again');
            }


            $res->setMessage($message)->setData($project);

        } catch (\Throwable $e) {
            $message = "Project ".$project->id." could not be created. Reason: ".$e->getMessage();
            $log = $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);
            return $res->setCode(409)->setMessage($e->getMessage())->send();
        }

        return $res->setCode(201)->send();


       /* $user = Auth::user();
        $res = new ApiMessage();

        $project = new Project();

        if(!$project)
        {
            $message = "An error occurred, please try again.";
            $log = $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);
            return $res->setCode(409)->setMessage($message)->send();
        }
        $message = "Project generated successfully by user: ".$user->id;
        $res->setMessage($message)->setData($project);
        $this->repo->LogStore($user->id,$user->role,'0',$message, $user->caption);
        return $res->send();*/
    }

}
