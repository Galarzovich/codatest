<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', 'AuthController@login');
Route::post('auth/signup', 'AuthController@signup');


Route::group(['middleware' => ['auth:api']], function(){


    Route::apiResource('project','ProjectController');

    Route::get('/client/fetch/{id}',"ClientController@show");
    Route::get('/client/list',"ClientController@filters");
    Route::delete('/client/remove/{id}',"ClientController@delete");
    Route::post('/client/save',"ClientController@store");
    Route::post('/client/assign-project',"ClientController@assignProject");
    Route::get('/client/dashboard',"ClientController@dash");


    Route::post('/project/save',"ProjectController@store");

    Route::group(['prefix' => 'users' ], function(){
        Route::get("me","UsersController@currentUser");
    });
});
